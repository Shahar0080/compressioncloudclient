package com.hit.objects.fromserver;

import com.hit.objects.Person;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to represent a file that a client is trying to upload
 **/
public class FileToUpload {
    private String fileName;
    private Person fileOwner;
    private String data;

    public FileToUpload(String fileName, Person fileOwner, String data) {
        this.fileName = fileName;
        this.fileOwner = fileOwner;
        this.data = data;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public Person getFileOwner() {
        return fileOwner;
    }

    public void setFileOwner(Person fileOwner) {
        this.fileOwner = fileOwner;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
