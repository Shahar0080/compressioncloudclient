package com.hit.objects.fromserver;

import com.hit.objects.FileType;
import com.hit.objects.Person;

import java.time.LocalDate;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class that represents a file the user can download
 **/
public class FileToDownload {
    private final long id;
    private String fileName;
    private LocalDate dateAdded;
    private FileType fileType;
    private String fileSize;
    private Person person;
    private String algorithmType;

    public FileToDownload(long id, String fileName, LocalDate dateAdded, FileType fileType, String fileSize, Person person, String algorithmType) {
        this.id = id;
        this.fileName = fileName;
        this.dateAdded = dateAdded;
        this.fileType = fileType;
        this.fileSize = fileSize;
        this.person = person;
        this.algorithmType = algorithmType;
    }

    public long getId() {
        return id;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public LocalDate getDateAdded() {
        return dateAdded;
    }

    public void setDateAdded(LocalDate dateAdded) {
        this.dateAdded = dateAdded;
    }

    public FileType getFileType() {
        return fileType;
    }

    public void setFileType(FileType fileType) {
        this.fileType = fileType;
    }

    public String getFileSize() {
        return fileSize;
    }

    public void setFileSize(String fileSize) {
        this.fileSize = fileSize;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getAlgorithmType() {
        return algorithmType;
    }

    public void setAlgorithmType(String algorithmType) {
        this.algorithmType = algorithmType;
    }
}
