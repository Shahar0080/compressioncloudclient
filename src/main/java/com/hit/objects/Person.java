package com.hit.objects;

import java.util.Objects;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: The person class to represent our users
 **/
public class Person {
    private static long count = 0;

    private long id;
    private final String firstName;
    private final String lastName;
    private final UserNameAndPassword userNameAndPassword;

    public Person(String firstName, String lastName, UserNameAndPassword userNameAndPassword) {
        this.id = count++;
        this.firstName = firstName;
        this.lastName = lastName;
        this.userNameAndPassword = userNameAndPassword;
    }

    public Person(Person person) {
        this.id = count++;
        this.firstName = person.firstName;
        this.lastName = person.lastName;
        this.userNameAndPassword = person.userNameAndPassword;
    }


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public UserNameAndPassword getUserNameAndPassword() {
        return userNameAndPassword;
    }

    public static void setCount(long count) {
        Person.count = count;
    }

    @Override
    public String toString() {
        return "Person{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", userNameAndPassword=" + userNameAndPassword +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person)) return false;
        Person person = (Person) o;
        return Objects.equals(getFirstName(), person.getFirstName()) &&
                Objects.equals(getLastName(), person.getLastName()) &&
                Objects.equals(getUserNameAndPassword(), person.getUserNameAndPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getUserNameAndPassword());
    }
}