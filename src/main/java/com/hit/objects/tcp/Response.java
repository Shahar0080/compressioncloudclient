package com.hit.objects.tcp;

/**
 * Author: Shahar Azar
 * Date:   27/12/2021
 * Purpose: A class to represent a client (TCP) Response
 **/
public class Response {
    private final String response;
    private final String errorMessage;

    public Response(String response) {
        this.response = response;
        this.errorMessage = null;
    }

    public Response(String response, String errorMessage) {
        this.response = response;
        this.errorMessage = errorMessage;
    }

    public String getResponse() {
        return response;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    @Override
    public String toString() {
        return "Response{" +
                "response='" + response + '\'' +
                ", errorMessage='" + errorMessage + '\'' +
                '}';
    }
}
