package com.hit.objects.tcp;

/**
 * Author: Shahar Azar
 * Date:   27/12/2021
 * Purpose: A class that represents a client (TCP) Request
 **/
public class Request {
    private String action;
    private String data;

    public Request(String action, String data) {
        this.action = action;
        this.data = data;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Request{" +
                "action='" + action + '\'' +
                ", data='" + data + '\'' +
                '}';
    }
}
