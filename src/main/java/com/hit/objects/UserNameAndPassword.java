package com.hit.objects;

import java.util.Objects;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class containing a username and a password
 **/
public class UserNameAndPassword {
    private final String userName;
    private final String password;

    public UserNameAndPassword(String userName, String password) {
        this.userName = userName;
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserNameAndPassword)) return false;
        UserNameAndPassword that = (UserNameAndPassword) o;
        return getUserName().equals(that.getUserName()) &&
                getPassword().equals(that.getPassword());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserName(), getPassword());
    }
}
