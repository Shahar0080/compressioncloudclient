package com.hit.objects;

import java.util.ArrayList;
import java.util.List;

/**
 * Author: Shahar Azar
 * Date:   20/11/2021
 * Purpose: A class that represents the file type (extension)
 **/
public enum FileType {
    TEXT("txt"),
    JSON("json"),
    XML("xml"),
    LOG("log");

    private final String extension;

    FileType(String extension) {
        this.extension = extension;
    }

    public static List<String> getAllEnums() {
        List<String> enums = new ArrayList<>();
        for (FileType type : FileType.values()) {
            enums.add(type.name());
        }
        return enums;
    }

    public String getExtension() {
        return extension;
    }

    public static List<String> getAllExtensions() {
        List<String> extensions = new ArrayList<>();
        for (FileType type : FileType.values()) {
            extensions.add(type.extension);
        }
        return extensions;
    }
}
