package com.hit.enums;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: The enum containing all of our scenes with their relevant data (title, fxml-path, width, height)
 **/
public enum SceneType {
    LOGIN_SCENE("CompressionCloud - התחברות", "login.fxml", 600, 400),
    SIGN_UP_SCENE("CompressionCloud - רישום", "sign-up.fxml", 600, 400),
    LOGGED_IN_SCENE("CompressionCloud - מסך ראשי", "logged-in.fxml", 1200, 800);

    private final String title;
    private final String resource;
    private final int width;
    private final int height;

    SceneType(String title, String resource, int width, int height) {
        this.title = title;
        this.resource = resource;
        this.width = width;
        this.height = height;
    }


    public String getTitle() {
        return title;
    }

    public String getResource() {
        return resource;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
