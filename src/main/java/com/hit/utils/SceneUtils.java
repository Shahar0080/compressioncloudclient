package com.hit.utils;

import com.hit.alerts.AlertDisplayer;
import com.hit.enums.SceneType;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * Author: Shahar Azar
 * Date:   20/11/2021
 * Purpose: A utility class for performing javafx/scene related operations
 **/
public class SceneUtils {

    private final static AlertDisplayer alertDisplayer = AlertDisplayer.getInstance();

    /**
     * A function to change the scene
     *
     * @param event     - the event that triggered this function
     * @param sceneType - the scene we want to change to - {@link SceneType}
     */
    public static void changeScene(ActionEvent event, SceneType sceneType) {

        Parent root;
        FXMLLoader loader;

        try {
            loader = new FXMLLoader(SceneUtils.class.getResource("/fxml/" + sceneType.getResource()));
            root = loader.load();
        } catch (Exception e) {
            e.printStackTrace();
            alertDisplayer.error("הבנתי", "change_scene_failure", "שגיאה כללית",
                    "אופס..", "איזשהי בעיה צצה לנו בעת מעבר למסך אחר :(");
            return;
        }

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setTitle(sceneType.getTitle());
        stage.setScene(new Scene(root, sceneType.getWidth(), sceneType.getHeight()));
        stage.centerOnScreen();
    }

}
