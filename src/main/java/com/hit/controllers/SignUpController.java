package com.hit.controllers;

import com.hit.alerts.AlertDisplayer;
import com.hit.data.DataHolder;
import com.hit.enums.SceneType;
import com.hit.objects.Person;
import com.hit.objects.UserNameAndPassword;
import com.hit.server.PureServerManager;
import com.hit.utils.SceneUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: The controller to let the user sign up
 **/
public class SignUpController implements Initializable {

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final PureServerManager pureServerManager = PureServerManager.getInstance();
    private final DataHolder dataHolder = DataHolder.getInstance();
    private final AlertDisplayer alertDisplayer = AlertDisplayer.getInstance();

    @FXML
    private TextField tf_first_name;

    @FXML
    private TextField tf_last_name;

    @FXML
    private TextField tf_username;

    @FXML
    private TextField tf_password_one;

    @FXML
    private TextField tf_password_two;

    @FXML
    private Button btn_sign_up;

    @FXML
    private Button btn_login;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        logger.info("initializing SignUpController");
        btn_sign_up.disableProperty().bind(
                (tf_first_name.textProperty().isEmpty())
                        .or(tf_last_name.textProperty().isEmpty())
                        .or(tf_username.textProperty().isEmpty())
                        .or(tf_password_one.textProperty().isEmpty())
                        .or(tf_password_two.textProperty().isEmpty())
        );

        tf_password_two.setOnAction(this::handleSignUpButton);
        btn_login.setOnAction((actionEvent) -> SceneUtils.changeScene(actionEvent, SceneType.LOGIN_SCENE));
    }

    @FXML
    private void handleSignUpButton(ActionEvent actionEvent) {
        logger.info("User attempting to sign-up");
        String errorsIntro = "לפני שאפשר להירשם, יש לתקן את הבעיות הבאות:\n";

        String errors = validateAllFieldsAndGetErrors();

        if (!errors.isEmpty()) {
            logger.info("User had errors while trying to sign-up. Errors: " + errors);
            alertDisplayer.error("הבנתי", "sign_up_input_invalid", "שגיאה בעת רישום",
                    "עלייך לתקן את השגיאות", errorsIntro + errors);
            return;
        }

        logger.info("Trying to sign-up user to server");
        UserNameAndPassword result = pureServerManager.performUserSignUp(new Person(tf_first_name.getText(), tf_last_name.getText(), new UserNameAndPassword(tf_username.getText(), tf_password_one.getText())));

        if (result == null) {
            logger.info("Could not perform sign-up for user " + tf_username.getText());
            alertDisplayer.error("הבנתי", "register_user_failure", "שגיאה כללית",
                    "אופס..", "לא הצלחנו לרשום אותך.. כדאי לנסות שוב :)");
            return;
        }


        logger.info("Successfully signed-up user " + tf_username.getText());
        dataHolder.setPerson(new Person(tf_first_name.getText(), tf_last_name.getText(),
                new UserNameAndPassword(tf_username.getText(), tf_password_one.getText())));
        alertDisplayer.info("יאללה", "sign_up_successful", "הרישום הצליח",
                "הצלחנו לרשום אותך :)", "שנתחיל..?");
        logger.info("--> changing scene to" + SceneType.LOGIN_SCENE);
        SceneUtils.changeScene(actionEvent, SceneType.LOGIN_SCENE);
    }

    /**
     * A function to validate all the fields.
     *
     * @return A String containing errors (if exist)
     */
    private String validateAllFieldsAndGetErrors() {
        StringBuilder errors = new StringBuilder();
        //~ first name      => can't be empty, max 50 chars
        String firstName = tf_first_name.getText();
        errors.append(showAndGetErrorFromTextField(tf_first_name, firstName.length() > 50, "שם פרטי"));

        //~ last name       => can't be empty, max 50 chars
        String lastName = tf_last_name.getText();
        errors.append(showAndGetErrorFromTextField(tf_last_name, lastName.length() > 50, "שם משפחה"));

        //~ userName        => can't be empty, max 50 chars, has to be unique(check in db)
        String userName = tf_username.getText();
        errors.append(showAndGetErrorFromTextField(tf_username, userName.length() > 50, "שם משתמש"));
        errors.append(checkUsernameAvailable(tf_username, !isUserNameAvailable(userName)));

        //~ passwordOne     => can't be empty, max 50 chars, has to be identical to other password (no other complexity cuz' f it)
        String passwordOne = tf_password_one.getText();
        errors.append(showAndGetErrorFromTextField(tf_password_one, passwordOne.length() > 50, "סיסמא"));

        //~ passwordTwo     => can't be empty, max 50 chars, has to be identical to other password (no other complexity cuz' f it)
        String passwordTwo = tf_password_two.getText();
        errors.append(showAndGetErrorFromTextField(tf_password_two, passwordTwo.length() > 50, "סיסמא"));
        errors.append(checkPasswordsMatch(tf_password_two, !passwordOne.equals(passwordTwo)));

        return errors.toString();
    }

    /**
     * A function to check if there is an error in the given text field
     *
     * @param textField - the given text field
     * @param isToShow  - A boolean that equals true if there is an error, else, false
     * @param fieldName - the fields name
     * @return - An empty String if no errors, else, a String containing the error
     */
    private String showAndGetErrorFromTextField(TextField textField, boolean isToShow, String fieldName) {
        if (isToShow) {
            textField.setStyle("-fx-border-color: #ff0000; -fx-border-width: 2px;");
            new animatefx.animation.Shake(textField).play();
            return fieldName + " - " + textField.getSelectedText() + " אינו טקסט תקין.\n";
        }
        textField.setStyle(null);
        return "";
    }

    /**
     * A function to check if there is an error in the given username text field
     *
     * @param textField - the given username text field
     * @param isToShow  - A boolean that equals true if there is an error, else, false
     * @return - An empty String if no errors, else, a String containing the error
     */
    private String checkUsernameAvailable(TextField textField, boolean isToShow) {
        if (isToShow) {
            textField.setStyle("-fx-border-color: #ff0000; -fx-border-width: 2px;");
            new animatefx.animation.Shake(textField).play();
            return "שם משתמש" + " - " + textField.getSelectedText() + " שם המשתמש כבר קיים.\n";
        }
        textField.setStyle(null);
        return "";
    }

    /**
     * A function to check if the password fields value match
     *
     * @param textField - the given text field
     * @param isToShow  - A boolean that equals true if there is an error, else, false
     * @return - An empty String if no errors, else, a String containing the error
     */
    private String checkPasswordsMatch(TextField textField, boolean isToShow) {
        if (isToShow) {
            textField.setStyle("-fx-border-color: #ff0000; -fx-border-width: 2px;");
            new animatefx.animation.Shake(textField).play();
            return "סיסמא" + " - " + textField.getSelectedText() + " הסיסמאות לא תואמות.\n";
        }
        textField.setStyle(null);
        return "";
    }

    /**
     * A function that checks if the chosen username is available against the server
     *
     * @param userName - the chosen username
     * @return - true if it is available, else false
     */
    private boolean isUserNameAvailable(String userName) {
        return pureServerManager.isUsernameAvailable(userName);
    }

}
