package com.hit.controllers;

import com.hit.alerts.AlertDisplayer;
import com.hit.data.DataHolder;
import com.hit.enums.SceneType;
import com.hit.io.FileManager;
import com.hit.objects.FileType;
import com.hit.objects.Person;
import com.hit.objects.fromserver.FileToDownload;
import com.hit.objects.fromserver.FileToUpload;
import com.hit.server.PureServerManager;
import com.hit.utils.SceneUtils;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.binding.Bindings;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.VBox;
import javafx.stage.FileChooser;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.net.URL;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: The controller for when the user logged in - un-formally the primary controller of our application
 **/
public class LoggedInController implements Initializable {

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final PureServerManager pureServerManager = PureServerManager.getInstance();
    private final DataHolder dataHolder = DataHolder.getInstance();
    private final FileManager fileManager = FileManager.getInstance();
    private final AlertDisplayer alertDisplayer = AlertDisplayer.getInstance();
    private final FileChooser fileChooser = new FileChooser();
    private AtomicBoolean isFileUploading = new AtomicBoolean(false);

    @FXML
    private VBox vbox_upload;

    @FXML
    private Label lbl_vbox_upload;

    @FXML
    private ImageView iv_refresh;

    @FXML
    private Label lbl_lastUpdated;

    @FXML
    private ImageView iv_user;

    @FXML
    private Label lbl_username;

    @FXML
    private VBox pnItems = null;

    @FXML
    private Button btnSignout;

    @FXML
    private Label lbl_totalUploaded;

    @FXML
    private Label lbl_uploadedByMe;

    @FXML
    private Label lbl_totalLZW;

    @FXML
    private Label lbl_totalGZIP;

    @FXML
    private Label lbl_totalNone;

    private final List<String> validExtensions = FileType.getAllExtensions();
    private Map<Long, FileToDownload> allFiles = new HashMap<>();
    private Person person;
    private final ObservableList<Node> list = FXCollections.observableArrayList();
    private Timeline periodicDataUpdate;

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        logger.info("initializing LoggedInController");
        person = dataHolder.getPerson();
        Bindings.bindContentBidirectional(list, pnItems.getChildren());
        periodicDataUpdate();
        setUserDataOnScreen();
        setStatisticsOnScreen();
        reAddAllRows();
        setLastUpdated(LocalDateTime.now());

        btnSignout.setOnAction(this::handleSignout);
        vbox_upload.setOnDragOver(this::dragOver);
        vbox_upload.setOnDragDropped(this::dragDropped);
        vbox_upload.setOnMouseClicked(this::vboxClicked);
        iv_refresh.setOnMouseClicked(this::refresh);
    }

    /**
     * A function to perform refresh on user click
     *
     * @param event - the action that triggered this function
     */
    protected void refresh(Event event) {
        logger.info("User chose to perform refresh - performing refresh now..");
        allFiles = getAllFiles();
        setUserDataOnScreen();
        setStatisticsOnScreen();
        reAddAllRows();
        setLastUpdated(LocalDateTime.now());
        logger.info("Finished performing refresh");
    }

    /**
     * A function to periodically ask the server for the updated file list, update all the files
     * and as an extension of that update all of the file-rows (since we bind it at @initialize)
     */
    private void periodicDataUpdate() {
        logger.info("Starting periodic data update");
        allFiles = getAllFiles();

        periodicDataUpdate = new Timeline(new KeyFrame(Duration.seconds(30), ev -> {
            logger.info("Periodic data update - tick");
            new Thread(() -> allFiles = getAllFiles()).start();
            reAddAllRows();
            setStatisticsOnScreen();
            setLastUpdated(LocalDateTime.now());
        }));
        periodicDataUpdate.setCycleCount(Animation.INDEFINITE);
        periodicDataUpdate.play();
    }

    /**
     * A function to get all the files we can download from the server
     *
     * @return - the files we can downloaded
     */
    private Map<Long, FileToDownload> getAllFiles() {
        logger.debug("Returning all files to download from server");
        return pureServerManager.getAllFiles();
    }

    /**
     * A function to update the logged-in user data on screen
     */
    private void setUserDataOnScreen() {
        logger.info("Starting to set user data on screen");
        String firstName = person.getFirstName();
        try {
            logger.info("Trying to set face for person with name: " + firstName);
            iv_user.setImage(new Image(getClass().getResourceAsStream("/img/faces/" + firstName + ".png")));
            logger.info("Successfully set face for person with name: " + firstName);
        } catch (Exception e) {
            logger.warn("Couldn't set face for person with name: " + firstName + " - face not found locally");
        }
        lbl_username.setText(firstName);
        logger.info("Finished setting user data on screen");
    }

    /**
     * A function to set the general cloud statistics on screen
     */
    private void setStatisticsOnScreen() {
        logger.info("Starting to set statistics on screen");
        Platform.runLater(() -> lbl_totalUploaded.textProperty().set(String.valueOf(allFiles.size())));

        long myFilesCount = allFiles
                .entrySet()
                .stream()
                .filter((entry) -> entry.getValue().getPerson().equals(person))
                .count();
        Platform.runLater(() -> lbl_uploadedByMe.textProperty().set(String.valueOf(myFilesCount)));

        long totalLZWCount = allFiles
                .entrySet()
                .stream()
                .filter((entry) -> entry.getValue().getAlgorithmType().equals("LZW"))
                .count();
        Platform.runLater(() -> lbl_totalLZW.textProperty().set(String.valueOf(totalLZWCount)));

        long totalGZIPCount = allFiles
                .entrySet()
                .stream()
                .filter((entry) -> entry.getValue().getAlgorithmType().equals("GZIP"))
                .count();
        Platform.runLater(() -> lbl_totalGZIP.textProperty().set(String.valueOf(totalGZIPCount)));

        long totalNoneCount = allFiles
                .entrySet()
                .stream()
                .filter((entry) -> entry.getValue().getAlgorithmType().equals("NONE"))
                .count();
        Platform.runLater(() -> lbl_totalNone.textProperty().set(String.valueOf(totalNoneCount)));

        logger.info("Finished setting statistics on screen");
    }

    /**
     * A function to re-add all of the rows to the list, and, as an extension of that, update all
     * the file-rows since we bind between the list & pnItems at @initialize
     */
    private void reAddAllRows() {
        logger.info("Starting to re-add all rows");
        list.clear();
        logger.info("Cleared the list");
        FileRowController row;
        if (allFiles.size() == 0) {
            list.add(new NoRowsController());
        } else {
            for (Map.Entry<Long, FileToDownload> entry : allFiles.entrySet()) {
                row = new FileRowController(this::refresh);
                row.setValues(entry.getValue());
                list.add(row);
            }
        }
        logger.info("Finished re-adding all rows");
    }

    /**
     * A function to let the user sign-out
     *
     * @param actionEvent - the action that triggered this function (here - button click)
     */
    public void handleSignout(ActionEvent actionEvent) {
        logger.info("User chose to sign-out");
        if (alertIfUploading("sign_out_blocked")) {
            return;
        }
        logger.info("--> User chose sign-out changing scene to " + SceneType.LOGIN_SCENE);
        dataHolder.setPerson(null);
        periodicDataUpdate.stop();
        SceneUtils.changeScene(actionEvent, SceneType.LOGIN_SCENE);
    }

    /**
     * A function to handle user dragging a file over our VBox
     * If the user drags a valid extension - consume what was dropped
     *
     * @param dragEvent - the event that triggered this function
     */
    public void dragOver(DragEvent dragEvent) {
        // On drag over if the DragBoard has files
        if (dragEvent.getGestureSource() != vbox_upload && dragEvent.getDragboard().hasFiles()) {
            logger.debug("User is dragging a file over our drag board");
            // All files on the drag-board must have an accepted extension
            if (!validExtensions.containsAll(
                    dragEvent.getDragboard().getFiles().stream()
                            .map(file -> getExtension(file.getName()))
                            .collect(Collectors.toList()))) {
                logger.warn("File does not have valid extension");
                dragEvent.consume();
                return;
            }

            // Allow for both copying and moving
            dragEvent.acceptTransferModes(TransferMode.COPY_OR_MOVE);
        }
        logger.debug("File has valid extension");
        dragEvent.consume();
    }

    /**
     * A function to handle user dropping a file to our VBox
     * Uploads the file to the cloud and shows a relevant alert (success/fail)
     *
     * @param dragEvent - the event that triggered this function
     */
    public void dragDropped(DragEvent dragEvent) {
        boolean success = false;
        if (dragEvent.getGestureSource() != vbox_upload && dragEvent.getDragboard().hasFiles()) {
            if (alertIfUploading("upload_drag_dropped_blocked")) {
                return;
            }
            logger.info("User has dropped a file to our drag board - now trying to upload");
            List<File> files = dragEvent.getDragboard().getFiles();
            if (files.size() > 1) {
                // only 1 file
                logger.info("User tried to upload more than one file");
                alertDisplayer.info("אישור", "upload_more_than_one_file", "לא ניתן לבצע את הפעולה",
                        "לא ניתן להעלות יותר מקובץ אחד.", "לא ניתן להעלות יותר מקובץ אחד בו-זמנית");
                return;
            }

            isFileUploading.set(true);
            displayLoading();
            new Thread(() -> {
                String fileAbsPath = files.get(0).getAbsolutePath();
                boolean isFileUploaded = pureServerManager.uploadFile(new FileToUpload(files.get(0).getName(),
                        dataHolder.getPerson(), fileManager.readData(fileAbsPath)));
                isFileUploading.set(false);
                if (isFileUploaded) {
                    Platform.runLater(() -> {
                        String description = "הקובץ הנבחר הועלה בהצלחה" + "\n" +
                                "הנתיב: " + fileAbsPath;
                        alertDisplayer.info("אישור", "upload_file_successful", "הקובץ הועלה בהצלחה",
                                "הקובץ הנבחר הועלה בהצלחה.", description);
                        refresh(dragEvent);
                    });
                    logger.info("Successfully uploaded user chosen file - path: " + fileAbsPath);
                } else {
                    Platform.runLater(() -> alertDisplayer.generalError("error_uploading_file"));
                    logger.error("Failed to upload user chosen file - path: " + fileAbsPath);
                }
            }).start();
            success = true;
        }
        dragEvent.setDropCompleted(success);
        dragEvent.consume();
    }

    /**
     * A function to get the extension of the given file
     *
     * @param fileName - the given file
     * @return A String containing the file extension
     */
    private String getExtension(String fileName) {
        logger.debug("Trying to parse extension of file with name of: " + fileName);
        String extension = "";

        int i = fileName.lastIndexOf('.');
        if (i > 0 && i < fileName.length() - 1) //if the name is not empty
            return fileName.substring(i + 1).toLowerCase();

        logger.debug("Successfully parsed extension of file with name of: " + fileName + ", extension: " + extension);
        return extension;
    }

    /**
     * A function to handle user clicking on our VBox
     * Opens a file picker so the user can choose a file to upload
     *
     * @param mouseEvent - the event that triggered this function
     */
    private void vboxClicked(MouseEvent mouseEvent) {
        logger.info("User clicked on our VBox (upload file)");
        if (alertIfUploading("upload_vbox_clicked_blocked")) {
            return;
        }
        configureFileChooser(fileChooser);
        File file = fileChooser.showOpenDialog(((Node) mouseEvent.getSource()).getScene().getWindow());
        if (file != null) {
            new Thread(() -> {
                String absFilePath = file.getAbsolutePath();
                logger.info("User chosen a file");
                isFileUploading.set(true);
                displayLoading();
                boolean isFileUploaded = pureServerManager.uploadFile(new FileToUpload(file.getName(),
                        dataHolder.getPerson(), fileManager.readData(file.getAbsolutePath())));
                isFileUploading.set(false);
                if (isFileUploaded) {
                    Platform.runLater(() -> {
                        String description = "הקובץ הנבחר הועלה בהצלחה" + "\n" +
                                "הנתיב: " + absFilePath;
                        alertDisplayer.info("אישור", "upload_file_successful    ", "הקובץ הועלה בהצלחה",
                                "הקובץ הנבחר הועלה בהצלחה.", description);
                        refresh(mouseEvent);
                    });
                    logger.info("Successfully uploaded file with path of: " + absFilePath);

                } else {
                    Platform.runLater(() -> alertDisplayer.generalError("error_uploading_file"));
                    logger.error("Couldn't upload file with path of: " + absFilePath);
                }
            }).start();
        } else {
            logger.info("User canceled upload file operation");
        }
    }

    /**
     * A function to configure our (upload) file chooser
     *
     * @param fileChooser - the file chooser
     */
    private void configureFileChooser(final FileChooser fileChooser) {
        logger.debug("Started configuring file chooser (upload file)");
        fileChooser.setTitle("Choose text files to upload..");
        fileChooser.setInitialDirectory(
                new File(System.getProperty("user.home"))
        );
        FileChooser.ExtensionFilter fileExtensions =
                new FileChooser.ExtensionFilter(
                        "Text files", "*.txt", "*.json", "*.xml", "*.log");

        fileChooser.getExtensionFilters().add(fileExtensions);
        logger.debug("Finished configuring file chooser (upload file)");
    }

    /**
     * A function to update the vbox text to let the user know it's loading
     */
    private void displayLoading() {
        String initialText = "Drag text files or click here to upload";
        String initialUploading = "Uploading";
        int dotsAmount = 10;


        new Thread(() -> {
            while (isFileUploading.get()) {
                StringBuilder loadingText = new StringBuilder(initialUploading);
                for (int i = 0; i < dotsAmount; i++) {
                    loadingText.append(".");
                    updateUploadedLabel(loadingText.toString());
                    try {
                        Thread.sleep(300);
                    } catch (Exception ignored) {

                    }
                    if (!isFileUploading.get()) {
                        break;
                    }
                }
            }
            updateUploadedLabel(initialText);
        }).start();

    }

    /**
     * A function to update the - vbox-uploaded-lbl without interfeering with the main thread
     *
     * @param text - the text to update
     */
    private void updateUploadedLabel(String text) {
        Platform.runLater(() -> lbl_vbox_upload.textProperty().set(text));
    }

    /**
     * A function to show and alert if a file is uploading
     *
     * @param alertTypeText - the alert type
     * @return true if in the middle of an upload, else, false
     */
    private boolean alertIfUploading(String alertTypeText) {
        if (isFileUploading.get()) {
            logger.info("Can't sign-out sign a file is being uploaded..");
            alertDisplayer.info("הבנתי", alertTypeText, "לא ניתן לבצע את הפעולה כרגע",
                    "אין אפשרות לבצע את הפעולה הזאת כרגע", "לא ניתן לבצע את הפעולה הזאת בעת העלאת קובץ..");
            return true;
        }
        return false;
    }

    /**
     * A function to set the last updated time
     *
     * @param time - the time to set
     */
    private void setLastUpdated(LocalDateTime time) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");
        lbl_lastUpdated.textProperty().set(dtf.format(time));
    }
}

