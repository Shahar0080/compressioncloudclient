package com.hit.controllers;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;

/**
 * Author: Shahar Azar
 * Date:   04/12/2021
 * Purpose: A class to represent the controller to show when there are no rows to show
 **/
public class NoRowsController extends AnchorPane {

    private final Logger logger = LogManager.getLogger(this.getClass());

    public NoRowsController() {
        logger.info("initializing NoRowsController");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/no-rows.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

    }
}
