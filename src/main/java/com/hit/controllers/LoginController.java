package com.hit.controllers;

import com.hit.alerts.AlertDisplayer;
import com.hit.data.DataHolder;
import com.hit.enums.SceneType;
import com.hit.objects.Person;
import com.hit.objects.UserNameAndPassword;
import com.hit.server.PureServerManager;
import com.hit.utils.SceneUtils;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URL;
import java.util.ResourceBundle;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: The controller to let the user log in
 **/
public class LoginController implements Initializable {

    private final Logger logger = LogManager.getLogger(this.getClass());
    private final PureServerManager pureServerManager = PureServerManager.getInstance();
    private final DataHolder dataHolder = DataHolder.getInstance();
    private final AlertDisplayer alertDisplayer = AlertDisplayer.getInstance();


    @FXML
    private Button btn_login;

    @FXML
    private Button btn_sign_up;

    @FXML
    private TextField tf_username;

    @FXML
    private TextField tf_password;

    public LoginController() {
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        logger.info("initializing LoginController");
        setUserAndPassIfDefined();
        btn_login.disableProperty().bind(
                tf_username.textProperty().isEmpty()
                        .or(tf_password.textProperty().isEmpty())
        );

        tf_username.setOnAction(this::handleLogin);
        tf_password.setOnAction(this::handleLogin);
        btn_login.setOnAction(this::handleLogin);
        btn_sign_up.setOnAction(this::handleSignUp);
    }

    /**
     * A function to set the username and password if it is defined in our {@link DataHolder}
     * it will be defined only after registration
     */
    private void setUserAndPassIfDefined() {
        logger.info("Starting to set user and password if defined");
        Person person = dataHolder.getPerson();
        if (person != null) {
            logger.info("user and password is defined");
            UserNameAndPassword userNameAndPassword = person.getUserNameAndPassword();
            tf_username.setText(userNameAndPassword.getUserName());
            tf_password.setText(userNameAndPassword.getPassword());
        } else {
            logger.info("user and password is NOT defined");
        }
    }

    /**
     * A function to handle the client clicking on the login button
     * Check if it's valid against the server - if it is, login
     * else - show a proper error alert
     *
     * @param actionEvent - the event that triggered this function
     */
    private void handleLogin(ActionEvent actionEvent) {
        logger.info("User is trying to login with the username: " + tf_username.getText());
        Person person = pureServerManager.performUserLogin(new UserNameAndPassword(tf_username.getText(), tf_password.getText()));
        if (person == null) {
            logger.info("User tried to login with wrong credentials");
            alertDisplayer.error("הבנתי", "login_input_invalid", "שגיאה בעת התחברות",
                    "מידע שגוי", "הפרטים שהזנת שגויים. אנא נסה שנית.");
            return;
        }
        logger.info("Username: " + tf_username.getText() + " approved.");
        dataHolder.setPerson(person);
        logger.info("--> changing scene to " + SceneType.LOGGED_IN_SCENE);
        SceneUtils.changeScene(actionEvent, SceneType.LOGGED_IN_SCENE);
    }

    /**
     * A function to handle user clicking on sign up button
     * Changes the scene to the SIGN_UP_SCENE
     *
     * @param actionEvent the event that triggered this function
     */
    private void handleSignUp(ActionEvent actionEvent) {
        logger.info("--> User chose sign-up changing scene to " + SceneType.SIGN_UP_SCENE);
        SceneUtils.changeScene(actionEvent, SceneType.SIGN_UP_SCENE);
    }

}
