package com.hit.controllers;

import com.hit.alerts.AlertDisplayer;
import com.hit.io.FileManager;
import com.hit.objects.FileType;
import com.hit.objects.Person;
import com.hit.objects.fromserver.FileToDownload;
import com.hit.server.PureServerManager;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.VBox;
import javafx.stage.DirectoryChooser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;

/**
 * Author: Shahar Azar
 * Date:   20/11/2021
 * Purpose: A class to represent each file row
 **/
public class FileRowController extends VBox {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private final PureServerManager pureServerManager = PureServerManager.getInstance();
    private final FileManager fileManager = FileManager.getInstance();
    private final AlertDisplayer alertDisplayer = AlertDisplayer.getInstance();

    private static final String STANDARD_BUTTON_STYLE = "-fx-background-color: #B7C3D7;";
    private static final String HOVERED_BUTTON_STYLE = "-fx-background-color: #8C8983;";
    private static final String ALL_BUTTONS_STYLE = "-fx-background-radius: 5;-fx-background-insets: 0;";

    private long id;

    @FXML
    private VBox fileRow;

    @FXML
    private Label lbl_fileName;

    @FXML
    private Label lbl_uploaded;

    @FXML
    private Label lbl_fileType;

    @FXML
    private Label lbl_fileSize;

    @FXML
    private Label lbl_user;

    @FXML
    private Label lbl_algorithmType;

    @FXML
    private Button btn_download;

    public FileRowController(Consumer<Event> refresh) {
        logger.info("initializing FileRowController");
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/file-row.fxml"));
        fxmlLoader.setRoot(this);
        fxmlLoader.setController(this);
        changeBackgroundOnHoverUsingEvents(fxmlLoader.getRoot());

        try {
            fxmlLoader.load();
        } catch (IOException exception) {
            throw new RuntimeException(exception);
        }

        setRightClick(refresh);
        btn_download.setOnAction(this::downloadFile);
    }

    /**
     * A function to set the right click options on the file row controller
     */
    private void setRightClick(Consumer<Event> refresh) {
        final ContextMenu contextMenu = new ContextMenu();
        contextMenu.getStyleClass().add("contextMenu");
        MenuItem delete = new MenuItem("Delete");
        delete.getStyleClass().add("contextMenuItem");
        contextMenu.getItems().addAll(delete);
        delete.setOnAction((event) -> deleteFile(event, refresh));

        fileRow.setOnMousePressed(event -> {
            if (event.isSecondaryButtonDown()) {
                contextMenu.show(fileRow, event.getScreenX(), event.getScreenY());
            }
        });

    }

    /**
     * A function to delete requested file by client (using file id)
     *
     * @param actionEvent - the action that triggered this function (here - Context click)
     */
    private void deleteFile(ActionEvent actionEvent, Consumer<Event> refresh) {
        logger.info("Client start delete file process.. file id: " + id);

        if (pureServerManager.deleteFile(id)) {
            Platform.runLater(() -> {
                String description = "הקובץ הנבחר נמחק בהצלחה.";
                alertDisplayer.info("אישור", "delete_file_success    ", "מחיקת הקובץ הצליחה",
                        "הקובץ הנבחר נמחק בהצלחה.", description);
                refresh.accept(actionEvent);
            });
            logger.info("Successfully deleted file for client with file id of " + id);
        } else {
            alertDisplayer.error("הבנתי", "delete_file_failure", "מחיקת הקובץ נכשלה",
                    "אופס..", "לא הצלחנו למחוק את הקובץ הנבחר..");
            logger.error("Error deleting file for client with file id of " + id);
        }
    }

    /**
     * A function to download requested file by client (using file id)
     * It opens a {@link DirectoryChooser} for the user to choose the directory he wants
     * to save the file at.
     * The file is saved with the initial file name and extension
     *
     * @param actionEvent - the action that triggered this function (here - Button click)
     */
    private void downloadFile(ActionEvent actionEvent) {
        logger.info("Client start download file process.. file id: " + id);
        DirectoryChooser dirChooser = new DirectoryChooser();

        dirChooser.setTitle("Select a folder to download to");

        File selectedDir = dirChooser.showDialog(((Node) actionEvent.getSource()).getScene().getWindow());

        File downloadedFile = new File(selectedDir.getAbsolutePath() + "/" + lbl_fileName.getText());

        String data = pureServerManager.downloadFile(id);

        boolean isSaved = fileManager.writeData(data, downloadedFile.getAbsolutePath());

        if (isSaved) {
            String description = "הקובץ הנבחר ירד בהצלחה" + "\n" +
                    "הנתיב: " + downloadedFile.getAbsolutePath();
            alertDisplayer.info("אישור", "download_file_success", "הורדת הקובץ הצליחה",
                    "הקובץ הנבחר ירד בהצלחה.", description);
            logger.info("Successfully downloaded file for client with file id of " + id);
        } else {
            alertDisplayer.error("הבנתי", "download_file_failure", "הורדת הקובץ נכשלה",
                    "אופס..", "לא הצלחנו להוריד את הקובץ הנבחר..");
            logger.error("Error downloading file for client with file id of " + id);
        }

    }

    /**
     * A function to change the background of the row whenever the user hovers
     * the mouse over it, for UX purposes.
     *
     * @param node - the file row
     */
    public void changeBackgroundOnHoverUsingEvents(final Node node) {
        logger.debug("Starting to change background hover for mouse");
        node.setStyle(STANDARD_BUTTON_STYLE + ALL_BUTTONS_STYLE);
        node.setOnMouseEntered(mouseEvent -> node.setStyle(HOVERED_BUTTON_STYLE + ALL_BUTTONS_STYLE));
        node.setOnMouseExited(mouseEvent -> node.setStyle(STANDARD_BUTTON_STYLE + ALL_BUTTONS_STYLE));
        logger.debug("Successfully changed background hover for mouse");
    }

    /**
     * A function to set all the necessary values for the file row, loaded using a {@link FileToDownload} object
     *
     * @param fileToDownload - the object that holds all the data we need
     */
    public void setValues(FileToDownload fileToDownload) {
        logger.info("Starting to set values for the file row");
        setFileName(fileToDownload.getFileName());
        setUploaded(fileToDownload.getDateAdded().format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
        setFileType(fileToDownload.getFileType());
        setFileSize(fileToDownload.getFileSize());
        setUser(fileToDownload.getPerson());
        setAlgorithmType(fileToDownload.getAlgorithmType());
        id = fileToDownload.getId();
        logger.info("Finished settings values for the file row");
    }

    /**
     * A function to set the file name of the file, on the lbl_fileName (using textProperty)
     *
     * @param fileName - the fileName
     */
    private void setFileName(String fileName) {
        logger.debug("Starting to set file name for lbl_fileName");
        lbl_fileName.textProperty().set(getFileNameWithoutExtension(fileName));
        logger.debug("Finished setting file name for lbl_fileName");
    }

    /**
     * A function to return a file name without it's extension
     *
     * @param fileName - the initial file name
     * @return - the file name without the extension if succeeded, else, the initial file name
     */
    private String getFileNameWithoutExtension(String fileName) {
        try {
            return fileName.split("\\.")[0];
        } catch (Exception e) {
            return fileName;
        }
    }

    /**
     * A function to set the uploaded date of the file, on the lbl_uploaded (using textProperty)
     *
     * @param uploaded the uploaded date
     */
    private void setUploaded(String uploaded) {
        logger.debug("Starting to set uploaded for lbl_uploaded");
        lbl_uploaded.textProperty().set(uploaded);
        logger.debug("Finished setting uploaded for lbl_uploaded");
    }

    /**
     * A function to set the file type of the file, on the lbl_fileType (using textProperty)
     *
     * @param fileType - the file type
     */
    private void setFileType(FileType fileType) {
        logger.debug("Starting to set file type for lbl_fileType");
        lbl_fileType.textProperty().set(fileType.getExtension());
        logger.debug("Finished setting file type for lbl_fileType");
    }

    /**
     * A function to set the file size of the file, on the lbl_fileSize (using textProperty)
     *
     * @param size - the size of the file
     */
    private void setFileSize(String size) {
        logger.debug("Starting to set file size for lbl_fileSize");
        lbl_fileSize.textProperty().set(prettifySize(size));
        logger.debug("Finished setting file size for lbl_fileSize");
    }

    /**
     * A function to "prettify" the file size
     *
     * @param size - the initial size
     * @return - a string describing the size in Bytes/KBytes/MBytes/GBytes
     */
    private String prettifySize(String size) {
        int sizeAsInt = Integer.parseInt(size);
        int kbs = sizeAsInt / 1024;
        int mbs = kbs / 1024;
        int gbs = mbs / 1024;

        if (gbs > 0) {
            return gbs + "GB";
        }

        if (mbs > 0) {
            return mbs + "MB";
        }

        if (kbs > 0) {
            return kbs + "KB";
        }

        return sizeAsInt + "B";
    }

    /**
     * A function to set the file owner of the file, on the lbl_user (using textProperty)
     *
     * @param person - the file owner
     */
    private void setUser(Person person) {
        logger.debug("Starting to set user name for lbl_user");
        lbl_user.textProperty().set(person.getUserNameAndPassword().getUserName());
        logger.debug("Finished setting user name for lbl_user");
    }

    /**
     * A function to set the algorithm type the file was compressed with, on the lbl_algorithmType (using textProperty)
     *
     * @param algorithmType - the algorithm type
     */
    private void setAlgorithmType(String algorithmType) {
        logger.debug("Starting to set algorithm type for lbl_algorithmType");
        lbl_algorithmType.textProperty().set(algorithmType);
        lbl_algorithmType.setAlignment(Pos.CENTER);
        logger.debug("Finished setting algorithm type for lbl_algorithmType");
    }

}
