package com.hit;

import com.hit.enums.SceneType;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: The main application class
 **/
public class Main extends Application {
    private final Logger logger = LogManager.getLogger(this.getClass());

    @Override
    public void start(Stage stage) throws Exception {
        logger.info("CompressionCloud client starting..");
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/" + SceneType.LOGIN_SCENE.getResource()));
        stage.getIcons().add(new Image(getClass().getResourceAsStream("/img/logos/compression_cloud_window.png")));
        stage.setTitle(SceneType.LOGIN_SCENE.getTitle());
        stage.setScene(new Scene(root, SceneType.LOGIN_SCENE.getWidth(), SceneType.LOGIN_SCENE.getHeight()));
        stage.setResizable(false);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}
