package com.hit.server;

import com.google.gson.reflect.TypeToken;
import com.hit.manager.JsonManager;
import com.hit.objects.Person;
import com.hit.objects.UserNameAndPassword;
import com.hit.objects.fromserver.FileToDownload;
import com.hit.objects.fromserver.FileToUpload;
import com.hit.objects.tcp.Request;
import com.hit.objects.tcp.Response;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.ConnectException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map;

/**
 * Author: Shahar Azar
 * Date:   25/12/2021
 * Purpose: A class to handle all the requests to the server using pure server
 **/
public class PureServerManager {
    private static PureServerManager instance = null;
    private final JsonManager jsonManager = JsonManager.getInstance();
    private final Logger logger = LogManager.getLogger(this.getClass());

    private final String DEFAULT_SERVER_URL = "localhost";
    private final int DEFAULT_SERVER_PORT = 25421;

    private final String REGISTER_PATH = "/user-register/";
    private final String IS_USERNAME_AVAILABLE_PATH = "/is-username-available/";
    private final String LOGIN_PATH = "/user-login/";

    private final String UPLOAD_FILE_PATH = "/upload-file/";
    private final String DOWNLOAD_FILE_PATH = "/download-file/";
    private final String DELETE_FILE_PATH = "/delete-file/";
    private final String GET_ALL_FILES_PATH = "/get-all-files/";

    private PureServerManager() {

    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static PureServerManager getInstance() {
        if (instance == null) {
            instance = new PureServerManager();
        }
        return instance;
    }

    /**
     * A function to send a request to the server with the given path
     *
     * @param request - the data to send the server
     * @return - the response of the server, if succeeded. else - null.
     */
    private Response send(Request request) {
        logger.info("Trying to send a message to the path: " + request.getAction());
        try {
            Socket s = new Socket(DEFAULT_SERVER_URL, DEFAULT_SERVER_PORT);

            // send data to server
            PrintWriter pw = new PrintWriter(s.getOutputStream());
            pw.println(jsonManager.toJson(request, Request.class));
            pw.flush();

            // get response from server
            InputStreamReader isr = new InputStreamReader(s.getInputStream());
            BufferedReader br = new BufferedReader(isr);
            String response = br.readLine();
            logger.info("Successfully sent a message to the path: " + request.getAction());

            s.close();
            // return the response
            return jsonManager.fromJson(response, Response.class);
        } catch (ConnectException connectException) {
            logger.error("Connection refused! Error sending a message to the path: " + request.getAction());
            connectException.printStackTrace();
        } catch (Exception e) {
            logger.error("General error! Error sending a message to the path: " + request.getAction());
            e.printStackTrace();
        }
        return null;
    }

    /**
     * A function to check if a given user name is available
     *
     * @param userName - the given user name
     * @return true if it's available, else, false
     */
    public boolean isUsernameAvailable(String userName) {
        logger.info("Trying to check if a user name is available");
        Response response = send(new Request(IS_USERNAME_AVAILABLE_PATH, userName));

        if (response == null || response.getErrorMessage() != null) {
            logger.error("Error checking if a user name is available, response: " + response);
            return false;
        }
        logger.info("Successfully checked if a user name is available");
        return jsonManager.fromJson(response.getResponse(), boolean.class);
    }

    /**
     * A function to perform user login
     *
     * @param userNameAndPassword - the username and password of the client
     * @return the {@link Person} if successful, else, null
     */
    public Person performUserLogin(UserNameAndPassword userNameAndPassword) {
        logger.info("Trying to perform user login");
        String userPassAsJson = jsonManager.toJson(userNameAndPassword, UserNameAndPassword.class);
        Response response = send(new Request(LOGIN_PATH, userPassAsJson));

        if (response == null || response.getErrorMessage() != null) {
            logger.error("Error performing user login, response: " + response);
            return null;
        }
        logger.info("Successfully performed user login");
        return jsonManager.fromJson(response.getResponse(), Person.class);
    }

    /**
     * A function to perform user sign up
     *
     * @param person - the {@link Person} containing all the needed relevant data to sign up
     * @return - the user name and password if succeeded, else, null
     */
    public UserNameAndPassword performUserSignUp(Person person) {
        logger.info("Trying to perform user sign up for person: " + person.toString());
        String personAsJson = jsonManager.toJson(person, Person.class);
        Response response = send(new Request(REGISTER_PATH, personAsJson));
        if (response == null || response.getErrorMessage() != null) {
            logger.error("Error performing user sign up");
            return null;
        }
        logger.info("Successfully performed user sign up");
        return jsonManager.fromJson(response.getResponse(), UserNameAndPassword.class);
    }

    /**
     * A function to upload a file to the server
     *
     * @param fileToUpload - the {@link FileToUpload} containing all the relevant data to upload a file
     * @return - true if succeeded, else, false
     */
    public boolean uploadFile(FileToUpload fileToUpload) {
        String fileName = fileToUpload.getFileName();
        logger.info("Trying to upload file. file name: " + fileName);
        Response response = send(new Request(UPLOAD_FILE_PATH, jsonManager.toJson(fileToUpload, FileToUpload.class)));
        boolean success = (response != null) && ((boolean) (jsonManager.fromJson(response.getResponse(), boolean.class)));
        if (success) {
            logger.info("Successfully uploaded the file. file name: " + fileName);
        } else {
            logger.error("Error uploading the file. file name: " + fileName);
        }
        return success;
    }

    /**
     * A function to download a file using it's id
     *
     * @param id - the id of the file
     * @return A String containing the file's data
     */
    public String downloadFile(long id) {
        logger.info("Trying to download file. file id: " + id);
        Response response = send(new Request(DOWNLOAD_FILE_PATH, String.valueOf(id)));

        if (response == null || response.getErrorMessage() != null) {
            logger.error("Error downloading file with id: " + id);
            return null;
        }
        logger.info("Successfully downloaded file. file id: " + id);
        return jsonManager.fromJson(response.getResponse(), String.class);
    }

    /**
     * A function to delete a file using it's id
     *
     * @param id - the id of the file
     * @return true if succeeded, else, false
     */
    public boolean deleteFile(long id) {
        logger.info("Trying to delete file. file id: " + id);
        Response response = send(new Request(DELETE_FILE_PATH, String.valueOf(id)));

        if (response == null || response.getErrorMessage() != null) {
            logger.error("Error deleting file with id: " + id);
            return false;
        }
        logger.info("Successfully deleted file. file id: " + id);
        return true;
    }

    /**
     * A function to get all the download-able files from the server, represented by {@link FileToDownload}
     *
     * @return - The list if succeeded, else, null
     */
    public Map<Long, FileToDownload> getAllFiles() {
        logger.info("Trying to get all files from server");
        Type type = new TypeToken<Map<Long, FileToDownload>>() {
        }.getType();

        Response response = send(new Request(GET_ALL_FILES_PATH, ""));

        if (response == null || response.getErrorMessage() != null) {
            logger.error("Error getting all files");
            return new HashMap<>();
        }
        logger.info("Successfully got all files from server");
        return jsonManager.fromJson(response.getResponse(), type);
    }
}
