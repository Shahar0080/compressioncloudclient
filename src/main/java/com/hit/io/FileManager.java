package com.hit.io;

/*
 *  Created by Shahar Azar on 14/08/2021
 *  Purpose: A class to manage files, including writing and reading them
 */

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to read/write from files
 **/
public class FileManager {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private static FileManager instance = null;

    private FileManager() {

    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static FileManager getInstance() {
        if (instance == null) {
            instance = new FileManager();
        }
        return instance;
    }


    /**
     * A function to read data from a given file path
     *
     * @param filePath - the given file path
     * @return - the file data if successful, else, an empty String
     */
    public String readData(String filePath) {
        logger.info("Starting to read data from file. file path: " + filePath);
        String fileContent = "";
        try {
            fileContent = new String(Files.readAllBytes(Paths.get(filePath)));
            logger.info("Successfully read data from file. file path: " + filePath);
        } catch (Exception e) {
            logger.error("Error reading data from file! - Given file path: " + filePath, e);
        }
        return fileContent;
    }

    /**
     * A function to write given data to a given file
     *
     * @param data     - the given data
     * @param filePath - the path to the given file
     * @return - true is succeeded, else, false
     */
    public boolean writeData(String data, String filePath) {
        logger.info("Starting to write data from to a file. file path: " + filePath);
        FileWriter fileWriter;
        try {
            fileWriter = new FileWriter(filePath);
            fileWriter.write(data);
            fileWriter.close();
            logger.info("Successfully read data from a file. file path: " + filePath);
            return true;
        } catch (IOException io) {
            logger.error("Error writing data to file! - Given file path: " + filePath, io);
        }
        return false;
    }
}
