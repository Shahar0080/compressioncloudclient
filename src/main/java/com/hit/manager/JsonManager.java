package com.hit.manager;

import com.google.gson.Gson;

import java.lang.reflect.Type;

/**
 * Author: Shahar Azar
 * Date:   16/11/2021
 * Purpose: A class to handle our gson formatting
 **/
public class JsonManager {
    private static JsonManager instance = null;
    private final Gson gson;

    private JsonManager() {
        gson = new Gson();
    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static JsonManager getInstance() {
        if (instance == null) {
            instance = new JsonManager();
        }
        return instance;
    }

    /**
     * A function to convert a object to a json
     *
     * @param object - the given object
     * @param type   - the given objects' type
     * @param <T>    - A signature to use generics in the function
     * @return the formatted object as json in a String
     */
    public <T> String toJson(T object, Type type) {
        return gson.toJson(object, type);
    }

    /**
     * A function to de-convert an object from json
     *
     * @param json - the given json
     * @param type - the given objects' type
     * @param <T>  - A signature to use generics in the function
     * @return the object
     */
    public <T> T fromJson(String json, Type type) {
        return gson.fromJson(json, type);
    }

}
