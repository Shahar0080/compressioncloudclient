package com.hit.alerts;

import com.hit.utils.SceneUtils;
import javafx.geometry.NodeOrientation;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonBar;
import javafx.scene.control.ButtonType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Author: Shahar Azar
 * Date:   20/11/2021
 * Purpose: A class to handle all alerts shown to the user
 **/
public class AlertDisplayer {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private static AlertDisplayer instance = null;

    private AlertDisplayer() {
    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static AlertDisplayer getInstance() {
        if (instance == null) {
            instance = new AlertDisplayer();
        }

        return instance;
    }


    public void info(String okButtonText, String alertTypeText, String title, String headerText, String description) {
        alert(okButtonText, alertTypeText, title, headerText, description, Alert.AlertType.INFORMATION);
    }

    public void info(String okButtonText, String alertTypeText, String title, String headerText, String description,
                     boolean isShowAndWait) {
        alert(okButtonText, alertTypeText, title, headerText, description, Alert.AlertType.INFORMATION, isShowAndWait);
    }

    public void error(String okButtonText, String alertTypeText, String title, String headerText, String description) {
        alert(okButtonText, alertTypeText, title, headerText, description, Alert.AlertType.ERROR);
    }

    public void error(String okButtonText, String alertTypeText, String title, String headerText, String description,
                      boolean isShowAndWait) {
        alert(okButtonText, alertTypeText, title, headerText, description, Alert.AlertType.ERROR, isShowAndWait);
    }


    private void alert(String okButtonText, String alertTypeText, String title, String headerText, String description,
                       Alert.AlertType alertType) {
        alert(okButtonText, alertTypeText, title, headerText, description, alertType, true);
    }

    private void alert(String okButtonText, String alertTypeText, String title, String headerText, String description,
                       Alert.AlertType alertType, boolean isShowAndWait) {
        logger.info("Showing alert with the following data {"
                + "okButtonText:" + okButtonText + ","
                + "alertTypeText:" + alertTypeText + ","
                + "title:" + title + ","
                + "headerText:" + headerText + ","
                + "description:" + description + ","
                + "alertType:" + alertType + ","
                + "isShowAndWait:" + isShowAndWait + "}");
        ButtonType confirm = new ButtonType(okButtonText, ButtonBar.ButtonData.OK_DONE);
        Alert alert = new Alert(alertType, alertTypeText, confirm);
        alert.getDialogPane().setNodeOrientation(NodeOrientation.RIGHT_TO_LEFT);
        alert.setTitle(title);
        alert.setHeaderText(headerText);
        alert.setContentText(description);
        Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
        stage.getIcons().add(new Image(SceneUtils.class.getResourceAsStream("/img/logos/compression_cloud.png")));
        if (isShowAndWait) {
            alert.showAndWait();
        } else {
            alert.show();
        }
    }

    public void generalError() {
        generalError("not-defined");
    }

    public void generalError(String label) {
        error("הבנתי", "general_error" + label, "שגיאה כללית",
                "אופס..", "קרתה לנו שגיאה כלשהי במערכת..");
    }


}
