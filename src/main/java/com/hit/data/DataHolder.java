package com.hit.data;

import com.hit.objects.Person;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Author: Shahar Azar
 * Date:   20/11/2021
 * Purpose: A class to hold data between controllers(scenes)
 **/
public class DataHolder {
    private final Logger logger = LogManager.getLogger(this.getClass());
    private static DataHolder instance = null;
    private Person person;

    private DataHolder() {
        person = null;
    }

    /**
     * A function to get the instance of the current class - implementing Singleton Design Pattern
     *
     * @return - the instance of the current class
     */
    public static DataHolder getInstance() {
        if (instance == null) {
            instance = new DataHolder();
        }
        return instance;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        logger.info("Received person data and setting it now");
        this.person = person;
    }
}
